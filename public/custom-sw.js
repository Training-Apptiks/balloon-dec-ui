self.addEventListener('push', event => {
  console.log("event pushed ::: ",event.push);
  const data = event.data.json()
  console.log('New notification', data)
  const options = {
    body: data.body,
  }
  event.waitUntil(
    self.registration.showNotification(data.title, options)
  );
})
