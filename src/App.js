// import logo from './logo.svg';
// import './App.css';
// import { useEffect, useState } from 'react';
// import axios from 'axios';
// import TextField from '@mui/material/TextField';

// function App() {

//   const[data,setData] = useState([]);
//   // const[name,setName] = useState('');
//   // const[user,setUser] = useState({});
//   // const[passFlag, setPassFlag] = useState(false);


//   useEffect(()=>{

//     axios.get('http://localhost:3000/jam/getjam').then(gh=>{
//       console.log("Response from api ", gh.data);
//       setData(gh.data)
//     })

//   },[])

//   return (
//     <div className="App">
//       <h1>JAM LIST </h1>
//       {
//         data.map(element => (
//           <h2>{element.name}</h2>
//         ))
//       }
//       <h1>Add JAM</h1>
//       JAM Id : <input></input>
//       JAM Name : <input></input> <br/>

//       <button>ADD JAM</button>
//       <hr/>

//       <TextField id="outlined-basic" label="Outlined" variant="outlined" />

//     </div>
//   );
// }

// export default App;



import Header from "./components/Header";
import { Routes, Route, Link } from "react-router-dom";
import Jam from './components/Jam';
import Plants from "./components/Plants";

export default function App() {
  return (
   <>
      <Header/>
      <Routes>
        <Route path="/jam" element={<Jam />} />
        <Route path="/plants" element={<Plants />} />

      </Routes>
   </>
  );
}
