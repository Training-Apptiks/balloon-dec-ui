import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import { Link } from "react-router-dom";

export default function Header() {
  return (
    <Box sx={{ flexGrow: 1}}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            APPTIKS
          </Typography>
          <Link to="/jam">
            <Button color="inherit">JAM</Button>
          </Link>
          <Link to="/plants">
            <Button color="inherit">PLANTS</Button>
          </Link>
          <Button color="inherit">CAKES</Button>
          <Button color="inherit">OCCASIONS</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}